# CoBox Colour styleguide 

Dark themed only

### Background colours:
#3f3f3f

#1F1F1F

#273327

### Borders
#FFF8F8

#f8fff8

#F8F8FF
 
### Font colours 
#FFF8F8

#FFF2F2

#f8fff8

Emphasis
#DDFFDD

Links

Visited links


### CoBox "Brand" colour:
#ACFFAC

### Pallette:
Generally strong contrast and large fonts for legibility (we like https://www.gov.uk/ for how they do that)

Bright, cheerful colours - draw further colours from the illustrations provided

Use gradients and textured fades

#92FF92

#C5FFC5

#FFCC33

#CC33FF

#6633FF

#3900E6
 
#FF6060

#FFACAC

#FFDFDF 