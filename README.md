# CoBox Styleguide

<div align="center">
  <img src="https://cobox.cloud/src/svg/logo.svg">
</div>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

## Table of Contents

- [About](#about)
- [Resources](#resources)
- [Examples](#examples)
- [Contributing](#contributing)
- [License](#license)

## About
**CoBox** is an encrypted p2p file system and distributed back-up tool. [README](https://gitlab.com/coboxcoop/readme) provides a map of the project.

`style-guide` is the home of the CoBox Style Guide. Here you will find the CoBox styles, fonts, shapes and colours for reference and use in web, presentations and print material that we are in the process of building. This is a living styleguide and will be regularly updated and changed.

## Resources
Web Content Accessibility Guidelines: https://www.w3.org/TR/WCAG20/

CoBox aims for level AA of WCAG 2.0


## Examples
Simple, dark themes

https://twochimpscoffee.com/
--> Like it because even though it is black it is very friendly, stylish and elegant

https://ethereum.org/
--> Like it because dark theme, and it shows the basic, necessary layout and sections and is simple, with very contained areas of more loose style, colours and drawings (in the logo)

https://www.gnu.org/gnu/gnu.html
--> Like it because it gives the necessary information without any fuss, has all the elements that are needed (including philosophy) and is clearly not a company but an open movement. And also all the languages makes a real difference for making it feel like a global project. 

### General feel
* Friendly
* Simple
* Analogue, "flat" 2d feel 
* not too much animation
* Feeling of a physical device rather than digital "otherworld"
* "Backgrounded" design with only very specific elements of explicit style that should be contained in boxes.


### Typography

#### Headers
h1 Alte Haas Grotesque, bold, 32px / 2em

#### Subheaders
h2 Alte Haas Grotesk, bold, 24px / 1.5em

h3 Alte Haas Grotesk, regular 24px  / 1.5em

h4 Alte Haas Grotesk, bold, italic 18px / 1.125em

#### body 
Alte Haas Grotesk, regular 18px/1.125em

#### links 
Underlined

#### emphasis
bold




### Colours
Dark themed only

#### Background colours:
#3f3f3f

#1F1F1F

#273327

#### Borders
#FFF8F8

#f8fff8

#F8F8FF
 
#### Font colours 
#FFF8F8

#FFF2F2

#f8fff8

Emphasis
#DDFFDD

Links

Visited links

#### CoBox "Brand" colour:
#ACFFAC

#### Colour pallette:
Generally strong contrast and large fonts for legibility (we like https://www.gov.uk/ for how they do that)

Bright, cheerful colours - draw from the illustrations provided

Use gradients and textured fades

#92FF92

#C5FFC5

#FFCC33

#CC33FF

#6633FF

#3900E6
 
#FF6060

#FFACAC

#FFDFDF 



### Voice
Casual, friendly

### Iconography


### Imagery
Generally flat, graphic and handdrawn (see the chimp coffee site above and the illustrations provided)
VERY limited use of actual photos (the misty valley provided)

### Forms

### Buttons

### Spacing
A lot of space - for legibility and accessibility.


### Dos and Donts
Only use animated icons for when something is loading
No unecessary animation (sliding, swooping etc)


## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

GPL-v3