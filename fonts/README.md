# Headers
h1 Alte Haas Grotesque, bold, 32px / 2em

## Subheaders
h2 Alte Haas Grotesk, bold, 24px / 1.5em

h3 Alte Haas Grotesk, regular 24px  / 1.5em

h4 Alte Haas Grotesk, bold, italic 18px / 1.125em

## body 
Alte Haas Grotesk, regular 18px/1.125em

## links 
Underlined

## emphasis
bold

